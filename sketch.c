#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "display.h"
#include "Instruction.h"
#include "Interpreter.h"

/**
 * \breif Tries to open the specified file in the specified mode, if
 * file cannot be open program terminates
 * \param filename The name of the file to open
 * \param mode The mode to open the file in, eg, "rb" for read binary
 * \note As usual, the returned file should be closed with fclose()
 * \return Pointer to the opened FILE
 */
FILE* tryOpen(char* filename, char* mode){
  FILE* f = fopen(filename, mode);
  if (f == NULL) {
      printf("Can't open %s\n", filename);
      exit(1);
  }
  return f;
}

///interprets a sketch file, drawing the contents to the screen
void runInterpret(char *filename, bool testing) {
    FILE* in = tryOpen(filename, "rb");

    display *d = newDisplay(filename, 200, 200, testing);
    interpretSketch(in, d);
    end(d);
    fclose(in);
    if (testing) printf("Sketch %s OK\n", filename);
}

///runs interactive REPL
void runREPL(){
  printf("                      REPL\n");
  printf("Type ':q' or press Ctrl-D or press Ctrl-C to quit\n");
  printf("-------------------------------------------------\n\n\n");
  display *d = newDisplay("REPL", 200, 200, false);

  Instruction ins;
  DrawState* ds = DrawState_Create();
  char lineIn[100];
  do{
    printf("      ");
    DrawState_Print(ds);
    printf("REPL> ");
    fflush(stdin);
    if(fgets(lineIn, 100, stdin) == &lineIn[0]){
      if(Instruction_Parse(&ins, lineIn)){
        //Instruction_Print(&ins);
        executeInstruction(&ins, ds, d);
      } else if (strcmp(lineIn, ":q") != 0){
        printf("Error parsing instruction: %s\n", lineIn);
      }
    } else {
      break;
    }
  }while(strcmp(lineIn, ":q") != 0);

  printf("\n\nREPL Quit\n");

  DrawState_Destroy(ds);
  end(d);
}

///compiles a sketch assembly file into a binary sketch file
void runCompiler(char* input, char* output){
  FILE* fin = tryOpen(input, "r");
  FILE* fout = tryOpen(output, "wb");

  printf("Compiling... ");
  Instruction ins;
  char lineIn[100];
  while(fgets(lineIn, 100, fin) == &lineIn[0]){
    if(Instruction_Parse(&ins, lineIn)){
      Instruction_PrintMachineCode(fout, &ins);
    }
  }
  printf("Done\n");

  fclose(fin);
  fclose(fout);
}

//decompiles a sketch binary file into a sketch assembly file
void runDecompiler(char* input, char* output){
  FILE* fin = tryOpen(input, "r");
  FILE* fout = tryOpen(output, "wb");

  printf("Decompiling... ");
  Instruction ins;
  while(Instruction_ReadNext(fin, &ins)){
    Instruction_PrintAsm(fout, &ins);
  }
  printf("Done\n");


  fclose(fin);
  fclose(fout);
}

void testCompiler(){
  //this test first decompiles all of the test .sketch files into assembly, then
  //compiles the generated assembly files back into .sketch files, it then runs
  //these against the .test files, decompiling and recompiling should not alter
  //the behaviour of the sketches!

  //note, this relies on all the ***.test files having a copy
  //named ***Test.test

  //This can be done on the terminal with the command:
  //for file in *.test; do cp "$file" "${file/.test/Test.test}"; done

  //You might want to test this does what it should with:
  //for file in *.test; do echo cp "$file" "${file/.test/Test.test}"; done

  runDecompiler("line.sketch", "lineTest.asm");
  runCompiler("lineTest.asm", "lineTest.sketch");

  runDecompiler("square.sketch", "squareTest.asm");
  runCompiler("squareTest.asm", "squareTest.sketch");

  runDecompiler("box.sketch", "boxTest.asm");
  runCompiler("boxTest.asm", "boxTest.sketch");

  runDecompiler("oxo.sketch", "oxoTest.asm");
  runCompiler("oxoTest.asm", "oxoTest.sketch");

  runDecompiler("diag.sketch", "diagTest.asm");
  runCompiler("diagTest.asm", "diagTest.sketch");

  runDecompiler("cross.sketch", "crossTest.asm");
  runCompiler("crossTest.asm", "crossTest.sketch");

  runDecompiler("clear.sketch", "clearTest.asm");
  runCompiler("clearTest.asm", "clearTest.sketch");

  runDecompiler("key.sketch", "keyTest.asm");
  runCompiler("keyTest.asm", "keyTest.sketch");

  runDecompiler("pauses.sketch", "pausesTest.asm");
  runCompiler("pausesTest.asm", "pausesTest.sketch");

  runDecompiler("field.sketch", "fieldTest.asm");
  runCompiler("fieldTest.asm", "fieldTest.sketch");

  runDecompiler("lawn.sketch", "lawnTest.asm");
  runCompiler("lawnTest.asm", "lawnTest.sketch");

  // Stage 1
  runInterpret("lineTest.sketch", true);
  runInterpret("squareTest.sketch", true);
  runInterpret("boxTest.sketch", true);
  runInterpret("oxoTest.sketch", true);

  // Stage 2
  runInterpret("diagTest.sketch", true);
  runInterpret("crossTest.sketch", true);

  // Stage 3
  runInterpret("clearTest.sketch", true);
  runInterpret("keyTest.sketch", true);

  // Stage 4
  runInterpret("pausesTest.sketch", true);
  runInterpret("fieldTest.sketch", true);
  runInterpret("lawnTest.sketch", true);

}

//tests sketches
void testSketches() {
    // Stage 1
    runInterpret("line.sketch", true);
    runInterpret("square.sketch", true);
    runInterpret("box.sketch", true);
    runInterpret("oxo.sketch", true);

    // Stage 2
    runInterpret("diag.sketch", true);
    runInterpret("cross.sketch", true);

    // Stage 3
    runInterpret("clear.sketch", true);
    runInterpret("key.sketch", true);

    // Stage 4
    runInterpret("pauses.sketch", true);
    runInterpret("field.sketch", true);
    runInterpret("lawn.sketch", true);

    printf("\nTESTING COMPILER/DECOMPILER...\n\n");
    testCompiler();
}

void printUsageHelp(){
  printf("Usage:\n");
  printf("    No Args                         | Run Tests\n");
  printf("    [filename]                      | Interpret the specified sketch file");
  printf("    -i [filename]                   | Interpret the specified sketch file\n");
  printf("    -repl                           | Run interactive interpreter, enter commands and see them drawn\n");
  printf("    -comp [inputFile] [outputFile]  | Compiles an 'assembly' sketch file into a 'machine code' sketch file\n");
  printf("    -dcomp [inputFile] [outputFile] | Decompiles a 'machine code' sketch file into an 'assembly' sketch file\n");
  printf("    -h                              | Display this help infomation\n");
}

int main(int n, char *args[n]) {
    if (n == 1) {
      testSketches();
    } else if (n == 2){
      if(strcmp(args[1], "-repl") == 0){
        runREPL(args[2]);
      } else if(strcmp(args[1], "-h") == 0){
        printUsageHelp();
      } else {
        runInterpret(args[1], false);
      }
    } else if (n == 3){
      if(strcmp(args[1], "-i") == 0){
        runInterpret(args[2], false);
      } else {
        printf("Invalid usage! ");
        printUsageHelp();
      }
    } else if (n == 4){
      if(strcmp(args[1], "-comp") == 0){
        runCompiler(args[2], args[3]);
      } else if(strcmp(args[1], "-dcomp") == 0){
        runDecompiler(args[2], args[3]);
      } else {
        printf("Invalid usage! ");
        printUsageHelp();
      }
    }
}
