#ifndef INTERPRETER_H
#define INTERPRETER_H

#include <stdio.h>
#include "display.h"
#include "Instruction.h"


struct DrawState;
typedef struct DrawState DrawState;

/**
 * \brief Creates a new default DrawState, required for executing an instruction
 * \note When finished with the returned DrawState delete it using DrawState_Destroy
 */
DrawState* DrawState_Create();

/**
 * \breif Destroys a DrawState instance created by DrawState_Create
 * \note Pointer ds should not be defrenced after this call as it will point
 * to freed memory!
 */
void DrawState_Destroy(DrawState* ds);

/**
 * \breif Prints human readable summary of the draw state to stdout
 */
void DrawState_Print(DrawState* ds);

/**
 * \breif Executes a single instruction, updating the dispay and draw state as required
 */
bool executeInstruction(Instruction* ins, DrawState* ds, display* d);

/**
 * \breif interprets the sketch in the specified file, drawing it to the specified
 * display
 */
void interpretSketch(FILE* in, display*d);

#endif
