#include "Instruction.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

const char* OPCODE_NAMES[] = {"DX", "DY", "DT", "PEN", "CLR", "KEY", "CR", "CG", "CB", "CA"};
const int OPCODE_MAX = 9;

const char* Instruction_GetOpcodeName(enum opcodeExt opcode){
  if(opcode > OPCODE_MAX){
    return "???";
  } else {
    return OPCODE_NAMES[opcode];
  }
}

enum opcodeExt Instruction_ParseOpcodeName(char* name){
  for(int i = 0; i <= OPCODE_MAX; ++i){
    if(strcmp(OPCODE_NAMES[i], name) == 0){
      return i;
    }
  }
  return OPEX_UNKNOWN;
}

bool Instruction_Parse(Instruction* result, char* line){
  char* cur = line;

  while(isspace(*cur)){
    ++cur;
    if(*cur == '\0'){ return false; }
  }


  char* opcodeStart = cur;
  while(!isspace(*cur) && *cur != '\0'){ ++cur; }
  bool more = (cur != '\0');
  *cur = '\0';
  enum opcodeExt opcode = Instruction_ParseOpcodeName(opcodeStart);

  if(opcode == OPEX_UNKNOWN){
    return false;
  }

  int operand = 0;
  if(more){
    //then more of sting to come, see if there is an operand
    char* temp;
    operand = strtol(++cur, &temp, 10);
    if(*temp != '\0' && *temp != '\r' && *temp != '\n'){
      //then not all of the string was converted
      return false;
    }
  }

  result->opcode = opcode;
  result->operand = operand;
  return true;
}

void Instruction_PrintAsm(FILE* stream, Instruction* ins){
  fprintf(stream, "%s %d\n", Instruction_GetOpcodeName(ins->opcode), ins->operand);
}

/**
 * \breif Finds the index of the first bit (starting from the msb) that is equal
 * to the specified bitValue
 * \param bitValue If true finds first 1, else finds first 0
 * \return Int representing index of the bit, lsb is index 0. If data contains
 * bits of only the value not being searched for returns 0
 */
int findIndexOfFirstBit(int data, bool bitValue){
  for(int i = sizeof(data)*8 - 1; i >= 0; --i){
    if((((data >> i) & 1) == 1) == bitValue){
      //then we found the first non-bitValue bit
      return i;
    }
  }
  //then data contains only the wrong type of bit, return 0
  return 0;
}

/**
 * \breif Writes a potentially multibyte value to some stream
 * The msb bit of each byte written will be a continuation bit indicating
 * whether more bytes are to follow. The minimum number of bytes possible will
 * be used by this function, each byte will contain a continuation bit and 7 bits
 * of the data to be written. More significant bits of the data are written first.
 * \note Despite this functions name if the data is small enough only a single byte
 * will be used, however this function is guarrentied to write at least one byte
 */
void writeMultibyte(FILE* stream, int data){
  //now deal with potential multibyte operand
  int numBits = 0;
  if(data > 0){
    //positive data, unused bits will be 0s, first 1 is the first bit in data
    //+1 as converting from bit index (starts at 0) to number of bits (starts at 1)
    numBits = findIndexOfFirstBit(data, true) + 1;

    //if we can represent a +ve in 7 bits the  we could use one byte, with continuation
    //bit of 0, but this doesnt work, as the top bit is a 1, so when read it is read as
    //-ve, hence we need an extra byte!
    //same for any other multiple of 7
    //in other words, if we have a multiple of 7 we need 1 extra bit for a leading 0
    //so it isnt interpreted as -ve
    if(numBits % 7 == 0){
      ++numBits;
    }

  } else {
    //then opeand negative, unused bits will be 1s, first 0 is first bit in data
    //+2 for two reasons:
    //  - first +1: converting from bit index (starts at 0) to number of bits (starts at 1)
    //  - second +1: we find the first 0, we want index of the '1' before it as this is
    //    the sign bit
    numBits = findIndexOfFirstBit(data, false) + 2;
  }
  if(numBits == 0){
    //this function is guarrentied to write at least one byte
    numBits = 7;
  } else {
    //we now know how many bits are going to be needed to represent the data
    //we need to round up to next multiple of 7
    if(numBits % 7 != 0){
      numBits += 7 - (numBits % 7);
    }
  }

  for(int i = numBits-7; i >= 0; i-=7){
    //if i == 0 then continuation bit is 0, else it is 1
    unsigned char byte = (i==0) ? 0 : 128;
    //set the bottom 7 bits to 7 bits of the operand
    byte |= ((data >> i) & 127);
    fputc(byte, stream);
  }
}

/**
 * \breif Reads a (potentially) mutlibyte piece of data from the specifed stream
 * The msb bit of each byte is the continuation bit, if one, another byte will be
 * read. The other 7 bits are part of the data, the more significant data bits
 * should be in earlier encountered bytes.
 * \param in The stream to read byte(s) from
 * \note The data read should be signed, the 2nd msb of the first byte will
 * determine the sign of the data, this sign will be extended to allow the data to
 * be stored in an int
 * \note Can only read up to 32 data bits
 * \note stream must have been opened in binary read mode
 */
int readMultibyte(FILE* in){
  int result;
  char nextChar = EOF;
  nextChar = fgetc(in);
  result = (nextChar & 127);
  int signMask;
  if(nextChar & 64){ //if top bit is 1 then -ve
    signMask = ~63; //start with 7 0s at end as always atleast 1 extra byte (if not one byte)
  } else {
    signMask = 0;
  }

  while((nextChar & 128) && nextChar != EOF){
    //shift existing bits along by 7 to make room for the new bits
    result <<= 7;
    //shift sign mask along, as only the unset bits need to be set to 1 if it is -ve
    signMask <<= 7;
    nextChar = fgetc(in);
    result |= (nextChar & 127);
  }
  result |= signMask;
  return result;
}

void Instruction_PrintMachineCode(FILE* stream, Instruction* ins){
  if(((ins->opcode == OPEX_DX || ins->opcode == OPEX_DY) && (ins->operand < 32 && ins->operand >= -32)) ||
     (ins->opcode == OPEX_DT && ins->operand >= 0 && ins->operand < 64)){
    //then this instruction is a simple 2 bit opcode and 6 bit operand, output 1 byte
    fputc(((ins->opcode & 3) << 6) | (63 & ins->operand), stream);
  } else {
    //then this instruction is not one of the basic three, or is but with an operand too large for 6 bits
    unsigned char insByte = 0;
    //the top two bits are 11, to indicate extended opcode
    insByte |= (3 << 6);

    //bottom 5 bits contain the opcode
    insByte |= ins->opcode & 31;

    if(ins->operand != 0){
      //then there is not an implied operand of 0, need extra bytes to store the
      //operand, set continuation bit to 1
      insByte |= 32;
    }

    //write the instruction byte
    fputc(insByte, stream);

    if(ins->operand != 0){
      writeMultibyte(stream, ins->operand);
    }
  }
}

bool Instruction_ReadNext(FILE* in, Instruction* result){
  char ins;
  ins = fgetc(in);
  if(ins == EOF){
    return false;
  }

  enum opcode opcode = ((ins >> 6 ) & 3);
  bool oneByte = true;

  if(opcode != OP_EXT){
    //then cast the opcode to an extended op code and store it in ins
    //as we already know the opcode
    result->opcode = (enum opcodeExt)opcode;
  } else {
    //then opcode isnt correct as its an extended opcode, look in bottom 5 bits
    result->opcode = ins & 31;

    if((ins >> 5) & 1){
      oneByte = false;
    }
  }

  if(oneByte){
    if(opcode == OP_EXT){
      //then one byte extended opcode, bottom bits form actual opcode
      //implied operand is 0
      result->operand = 0;
    } else {
      //then a standard opcode, operand is the bottom 6 bits of the byte
      result->operand = ins & 63;

      //if instruction is DX or DY then opcode is signed, hence need to extend the
      //top bit of the 6 bit opcode into the whole 32 bits (or however many bits int
      //is on the computer)
      //if the opcode is DT then the opcode is unsigned, dont touch!
      if((result->opcode != OPEX_DT) && ((result->operand & 32))){
        //we want all bits above 6 to be 1, or it with ...111111000000
        result->operand |= ~63;
      }
    }
  } else {
    //then read next n bytes to find the operand
    result->operand = readMultibyte(in);
  }

  return true;
}
