#ifndef INSTRUCTION_H
#define INSTRUCTION_H

#include <stdio.h>
#include <stdbool.h>

///Enumeration of the four basic 2 bit opcodes
enum opcode{
  OP_DX = 0,
  OP_DY = 1,
  OP_DT = 2,
  OP_EXT = 3,
};


///Enumeration containing the full set of extended opcodes
enum opcodeExt{
  OPEX_DX = 0,
  OPEX_DY = 1,
  OPEX_DT = 2,
  OPEX_PEN = 3,
  OPEX_CLEAR = 4,
  OPEX_KEY = 5,
  OPEX_COLOR_R = 6,
  OPEX_COLOR_G = 7,
  OPEX_COLOR_B = 8,
  OPEX_COLOR_A = 9,

  //represents an unknown opcode, returned by Instruction_GetOpcodeName if
  //abreivation is invalid
  OPEX_UNKNOWN = 31,
};

/**
 * \breif Stores the data for a single sketch instruction
 */
typedef struct{
    //The opcode for this instruction
    //This is the most significant 2 bits of a byte
    //if OP_DX, OP_DY, or OP_DT then the union's
    //operand member should be used
    //if set to OP_EXT then the union's extOp member
    //should be used
    enum opcodeExt opcode;

    //the operand for the operator to perform
    //note that this variable is used for both regular and extended opcodes
    //if a regular opcode is used then this int is the last 6 bits of the
    //instruction
    //if an extended opcode is used then this is either 0 or is made from
    //the next n bytes following the instruction
    //int operand;
    int operand;
} Instruction;


/**
 * \breif Prints assembly version of instruction to the specified stream
 */
void Instruction_PrintAsm(FILE* stream, Instruction* ins);

/**
 * \breif Prints machine code version of instruction to the specified stream
 */
void Instruction_PrintMachineCode(FILE* stream, Instruction* ins);

/**
 * \breif Returns the 2 or 3 character name of the specified opcode
 */
const char* Instruction_GetOpcodeName(enum opcodeExt opcode);

/**
 * \breif Parses a 2 or 3 character name of an extended opcode
 * \return the opcodeExt associated with the abreivation
 * \note Returns OPEX_UNKNOWN if the name is not recognised
 */
enum opcodeExt Instruction_ParseOpcodeName(char* name);

/**
 * \breif Parses an instruction storing the result in the param result
 * String line must be in the form:
 *   [opcodeName] [operand]
 *   or
 *   [opcodeName]
 *  If no operand is specified it is assumed to be a 0
 * \note Modifies the param result, if true is returned
 * \return True if the instruction was successfully parsed
 */
bool Instruction_Parse(Instruction* result, char* line);


/**
 * \breif Sets the values of the specified instruction such that they represnet
 * the next instruction in the file in
 * \note modifies the Instruction struct result
 * \return bool - False if there is no next instruction (eg, End of file reached)
 * else, true
 */
bool Instruction_ReadNext(FILE* in, Instruction* result);

#endif
