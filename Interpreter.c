#include <stdbool.h>
#include <stdlib.h>
#include "Interpreter.h"


///rgba value for opaque black
const int COLOR_BLACK = 255;

/**
 * \breif Contains data related to current state when drawing a picture
 */
struct DrawState{
  //if true the pen is up and drawing should not occur
  bool penUp;

  //the current x and y
  int curX, curY;

  //the x and y at the end of the previous drawing operation
  int lastX, lastY;

  //current color, used when something is drawn. Stored as rgba, 8 bits per component
  int color;
} ;

DrawState* DrawState_Create(){
  DrawState* ds = malloc(sizeof(DrawState));
  ds->penUp = true;
  ds->curX = 0;
  ds->curY = 0;
  ds->lastX = 0;
  ds->lastY = 0;
  ds->color = COLOR_BLACK;
  return ds;
}


void DrawState_Destroy(DrawState* ds){
  free(ds);
}

void DrawState_Print(DrawState* ds){
  printf("Pen: %s   Old Pos: (%d, %d)    Cur Pos: (%d, %d)    Color: (%d, %d, %d, %d)\n",
    (ds->penUp ? " UP " : "DOWN"),
    ds->lastX, ds->lastY,
    ds->curX, ds->curY,
    (ds->color >> 24) & 255,
    (ds->color >> 16) & 255,
    (ds->color >>  8) & 255,
    (ds->color      ) & 255
    );
}

void flushDrawing(DrawState* ds, display* d){
  if(!ds->penUp && (ds->lastX != ds->curX || ds->lastY != ds->curY)){
    if(ds->color == COLOR_BLACK){
      line(d,ds->lastX, ds->lastY, ds->curX, ds->curY);
    } else {
      cline(d,ds->lastX, ds->lastY, ds->curX, ds->curY, ds->color);
    }
  }
  ds->lastX = ds->curX;
  ds->lastY = ds->curY;
}

/**
 * \breif sets the value of the specified byte in some int
 * \param original The int to modify
 * \index The index of the byte to modify, 0 is the least significant byte
 * 3 is the most significant byte
 * \param byte The new value for the specified byte
 * \return original with the specified byte set to the param byte
 */
int setIntByte(int original, char index, unsigned char byte){
  original &= ~(0xFF << index*8);
  original |= (byte << index*8);
  return original;
}

bool executeInstruction(Instruction* ins, DrawState* ds, display* d){
  //Instruction_Print(ins);
  switch(ins->opcode){
    case OPEX_DX:
      ds->curX += ins->operand;
      return true;
    case OPEX_DY:
      ds->curY += ins->operand;
      return true;
    case OPEX_DT:
      flushDrawing(ds, d);
      pause(d, ins->operand);
      return true;
    case OPEX_PEN:
      flushDrawing(ds, d);
      ds->penUp = !ds->penUp;
      return true;
    case OPEX_CLEAR:
      clear(d);
      return true;
    case OPEX_KEY:
      key(d);
      return true;
    case OPEX_COLOR_R:
      ds->color = setIntByte(ds->color, 3, ins->operand);
      return true;
    case OPEX_COLOR_G:
      ds->color = setIntByte(ds->color, 2, ins->operand);
      return true;
    case OPEX_COLOR_B:
      ds->color = setIntByte(ds->color, 1, ins->operand);
      return true;
    case OPEX_COLOR_A:
      ds->color = setIntByte(ds->color, 0, ins->operand);
      return true;
    default:
      return false;
  }
}

void interpretSketch(FILE* in, display*d){
  DrawState* state = DrawState_Create();
  Instruction ins;

  while(Instruction_ReadNext(in, &ins)){
    if(!executeInstruction(&ins, state, d)){
      printf("ERROR, BAD INSTRUCTION");
      break;
    }
  }

  DrawState_Destroy(state);
}
